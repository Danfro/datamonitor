#include <QDebug>
#include <QCoreApplication>
#include <QFileInfo>
#include <iostream>
#include <fstream>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>

#include "installdaemon.h"

using namespace std;

Installdaemon::Installdaemon() :
    m_installProcess(),
    m_uninstallProcess(),
    m_installIndProcess(),
    m_uninstallIndProcess()
{
    connect(&m_installProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onInstallFinished(int, QProcess::ExitStatus)));
    connect(&m_uninstallProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onUninstallFinished(int, QProcess::ExitStatus)));
    connect(&m_installIndProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onInstallIndFinished(int, QProcess::ExitStatus)));
    connect(&m_uninstallIndProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onUninstallIndFinished(int, QProcess::ExitStatus)));

    checkInstalled();
    checkIndicatorInstalled();
}

void Installdaemon::install() {
    //TODO don't hardcode this
    m_installProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/install.sh");
}

void Installdaemon::installInd() {
    //TODO don't hardcode this
    m_installIndProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/indicator/installIndicator.sh");
}

void Installdaemon::uninstall() {
    //TODO don't hardcode this
    m_stopDaemon.start("stop mattdaemon");
    m_uninstallProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/uninstall.sh");
}

void Installdaemon::uninstallInd() {
    //TODO don't hardcode this
    m_uninstallIndProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/indicator/uninstallIndicator.sh");
}

void Installdaemon::onInstallFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "Daemon install finished";
    qDebug() << "stdout" << m_installProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_installProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkInstalled();
    Q_EMIT installed(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

void Installdaemon::onInstallIndFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "Indicator install finished";
    qDebug() << "stdout" << m_installIndProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_installIndProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkIndicatorInstalled();
    Q_EMIT installedIndicator(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

void Installdaemon::onUninstallFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "Daemon uninstall finished";

    qDebug() << "stdout" << m_uninstallProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_uninstallProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkInstalled();
    Q_EMIT uninstalled(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

void Installdaemon::onUninstallIndFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "Indicator uninstall finished";

    qDebug() << "stdout" << m_uninstallIndProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_uninstallIndProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkIndicatorInstalled();
    Q_EMIT uninstalledIndicator(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

bool Installdaemon::checkInstalled() {
    QFileInfo session("/home/phablet/.config/upstart/mattdaemon.conf");
    QFileInfo sessionService("/home/phablet/.config/upstart/mattdaemon-service.conf");

    m_isInstalled = session.exists() && sessionService.exists();
    Q_EMIT isInstalledChanged(m_isInstalled);

    return m_isInstalled;
}

bool Installdaemon::checkIndicatorInstalled() {
    QFileInfo session("/home/phablet/.config/upstart/mbellei-indicator-datamonitor.conf");
    QFileInfo indicator("/home/phablet/.local/share/unity/indicators/com.mbellei.indicator.datamonitor");

    m_isIndInstalled = session.exists() && indicator.exists();
    Q_EMIT isIndicatorInstalledChanged(m_isIndInstalled);

    return m_isIndInstalled;
}

void Installdaemon::storeTimeStep(const QString tstep) {
  string tstepString = tstep.toStdString();
  string command = "echo ";
  command = command + tstepString + " > /home/phablet/.config/datamonitor.matteobellei/daemonIdle.conf";
  FILE* pipe = popen(command.c_str(), "r");
}

void Installdaemon::traslationNotif(const QString string1, const QString string2, const QString string3) {
  QJsonObject object;
  object.insert("1", QJsonValue("<" + string1 + "<"));
  object.insert("2", QJsonValue("<" + string2 + "<"));
  object.insert("3", QJsonValue("<" + string3 + "<"));
  QJsonDocument doc;
  doc.setObject(object);
  QFile config(m_configPath + "translationNotif.json");
  config.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
  config.write(doc.toJson());
  config.close();
}
