#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "installdaemon.h"

void InstalldaemonPlugin::registerTypes(const char *uri) {
    //@uri Pluginname
    qmlRegisterSingletonType<Installdaemon>(uri, 1, 0, "Installdaemon", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Installdaemon; });
}
