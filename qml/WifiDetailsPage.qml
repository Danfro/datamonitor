import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Readconns 1.0

Page {
    id: wifiDetailsPage

    property var connElements : [];
    property var wifiProps : [];
    property var deviceNames : [];

    header: PageHeader {
        title: i18n.tr("WI-FI details")

        StyleHints	{
          foregroundColor: fontColor
          backgroundColor: bkgColor
          dividerColor:	lineColor
        }
    }

    Component.onCompleted: {
        deviceNames = Readconns.deviceList()
        wifiProps = Readconns.wifiProps(deviceNames)
        connElements = Readconns.connWifiDetails(wifiProps[1])
    }

    Timer {
       running: true;
       interval: 10000;
       repeat: true;
       onTriggered: {
          connElements = Readconns.connWifiDetails(labelConnName.text);
       }
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    ScrollView {
        id: scrollView
        anchors {
            top: wifiDetailsPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        Column {
            id: wifiDetailsColumn
            spacing: units.gu(2)
            width: scrollView.width

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: genDeviceName
                    title {
                        text: i18n.tr("General device name:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelGenDevName
                        text: wifiProps[3]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: driverVersion
                    title {
                        text: i18n.tr("Driver version:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        text: wifiProps[2]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: connectionName
                    title {
                        text: i18n.tr("Connection name:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelConnName
                        text: wifiProps[1]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: connType
                    title {
                        text: i18n.tr("Connection type:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelDevName
                        text: wifiProps[0]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: connSecurity
                    title {
                        text: i18n.tr("Connection security:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        text: connElements[8]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: connVelocity
                    title {
                        text: i18n.tr("Connection velocity:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        text: (connElements[4] == "--") ? "--" : (connElements[4] + " " + connElements[5])
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: connChannel
                    title {
                        text: i18n.tr("Connection channel:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        text: connElements[3]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: conneStrength
                    title {
                        text: i18n.tr("Connection strength:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        text: isNaN(connElements[6]) ? "--" : connElements[6]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ProgressBar {
                id: determinateBar
                enabled: isNaN(connElements[6]) ? false : true
                minimumValue: 0
                maximumValue: 100
                value: isNaN(connElements[6]) ? 0 : connElements[6]
            }

        }
    }
}
