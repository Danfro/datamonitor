import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3
import Installdaemon 1.0

/* Thanks to: https://github.com/jwintz/qchart.js for QML bindings for Charts.js */
import "QChart.js" as Charts

/* replace the 'incomplete' QML API U1db with the low-level QtQuick API */
import QtQuick.LocalStorage 2.0

/* Import our javascript files */
import "ChartUtils.js" as ChartUtils
import "DateUtils.js" as DateUtils
import "Storage.js" as Storage
import "StorageThresholds.js" as StorageThresholds


Page {
     id: chartPage
     visible: false

     /* default is today, after is updated when the user chose a date with the TimePicker */
     property double summitArrowOffset: 14.43375673
     property string pageHeaderTitle : "SIM analytic page";
     property string targetDate : Qt.formatDateTime(new Date(), "yyyy-MM-dd");
     property var objects : [];
     property double maxScaleGraph : settings.maxScaleY;
     property double maxScaleGraphStored: 0;
     property string typeData: "MBytes"
     property string colorLabelThresh: "transparent"
     property string graphXLabel: "Days of Month"
     property alias notification: notificationLoader.item
     property double xbubble: 0
     property double ybubble: 0
     property var firstDayMonth
     property var lastDayMonth
     property double totalDataEdit: 0
     property double animationXfR: 0
     property double animationXtR: 0
     property double animationXfL: 0
     property double animationXtL: 0
     property bool rightMove: false
     property int durationAnimation: 1000

     Loader {
         id: notificationLoader

         x: xbubble
         y: ybubble
         z: 1

         asynchronous: true
         visible: status == Loader.Ready

         Component.onCompleted: {
             setSource(Qt.resolvedUrl("../qml/MessageBubble.qml"), {
                           bubbleOffset: units.gu(8)
                       })
         }
     }

     header: PageHeader {
          title: settings.dataType + ": " + Qt.formatDateTime(chartPage.targetDate, "MMMM yyyy")

          StyleHints	{
            foregroundColor: fontColor
            backgroundColor: bkgColor
            dividerColor: lineColor
          }

          trailingActionBar.actions: [
          Action {
              iconName: "calendar"
              onTriggered: PopupUtils.open(popoverTargetMonthPicker, chartPage.header)
          }
          ]
     }

     Rectangle {
         color: bkgColor
         width: parent.width
         height: parent.height
     }

     Timer {
        running: true;
        interval: 10000;
        repeat: true;
        onTriggered: drawGraph(settings.dataType);
     }

     ParallelAnimation {
          id: swipeAnimationSequence
          running: false
          NumberAnimation {
            id: moveGraphToRight
            target: mBytesChart
            property: "x"
            easing.type: Easing.OutBack //Easing.InOutQuad
            from: animationXfR
            to: animationXtR
            duration: durationAnimation
          }
          NumberAnimation {
            id: moveGraphfromLeft
            target: mBytesChart
            property: "x"
            easing.type: Easing.OutBack //Easing.InOutQuad
            from: animationXfL
            to: animationXtL
            duration: durationAnimation
          }
          onRunningChanged: {
              if (running) {
                  drawGraph(settings.dataType)
              }
          }
     }

     Component.onCompleted: {

        graphXLabel = i18n.tr("Days of Month");
        if (settings.dataType == "WIFI") {
            pageHeaderTitle =  i18n.tr("Wi-fi analytic page");
            maxScaleGraphStored = settings.maxScaleYStored;
            totalDataEdit = settings.totalWIFI;
        } else {
            pageHeaderTitle =  i18n.tr("SIM analytic page");
            maxScaleGraph = settings.maxScaleYSIM;
            maxScaleGraphStored = settings.maxScaleYSIMStored;
            totalDataEdit = settings.totalSIM;
        }

        drawGraph(settings.dataType);

        var numStrings = StorageThresholds.getNumData(settings.dataType);
        for (var i=0; i < numStrings; i++) {
            var mbData = StorageThresholds.getValueFromPos(i, settings.dataType);
            var yObj = (maxScaleGraph*(settings.axisY-settings.errorY1) - mbData*(settings.axisY-settings.errorY1-settings.errorY2))/maxScaleGraph-25;
            createObj(yObj, mbData);
            initOrdDataModel(mbData);
        }
        if (numStrings==0) {
            colorLabelThresh=fontColor;
        }
     }

     ListModel {
         id: dataModel
     }

     Column{
        id: chartPageMainColumn
        spacing: units.gu(2)
        anchors {
            top: chartPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
            /* Create a PopOver containing a DatePicker */
            Component {
                id: popoverTargetMonthPicker
                Popover {
                    id: popoverDatePicker

                    DatePicker {
                        id: timePicker
                        mode: "Months|Years"
                        date: new Date()
                        minimum: {
                            var time = new Date()
                            time.setFullYear('2000')
                            return time
                        }
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            chartPage.targetDate = Qt.formatDateTime(timePicker.date, "yyyy-MM-dd");
                            drawGraph(settings.dataType);
                        }
                    }
                }
            }

        //---------------- Chart ---------------------
        Grid {
            id: chartGridContainer
            visible: true
            columns:2                     // this property doesn't seem to be effective on the chart
            columnSpacing: units.gu(1)    // this property doesn't seem to be effective on the chart
            width: parent.width;
            height: parent.height;

            Rectangle {
                id: mBytesChartContainer
                visible: true    //Ubuntu.Components.Themes.Ambiance/SuruDark/SuruGradient
                color: bkgColor
                width: parent.width;
                height: parent.height - bottomEdgeHint.height/2;

                property string moveYsteps;
                property double maxValue: maxScaleGraph;
                property double maxValueStored: maxScaleGraphStored;

                /* The monthly MBytes chart */
                QChart{
                    id: mBytesChart;
                    width: parent.width;
                    height: parent.height;
                    chartAnimated: false;
                    /* for all the options see: QChart.js */
                    chartOptions: {"barStrokeWidth": 0,
                                   "scaleFontColor": fontColor,
                                   "scaleLineColor": lineColor,
                                   "scaleGridLineColor": gridColor,
                                   "scaleMaxAxisYvalue": mBytesChartContainer.maxValue,
                                   "scaleMaxAxisYStored": mBytesChartContainer.maxValueStored,
                                   "XLabel": chartPage.graphXLabel
                                  }
                    /* chartData: set when the user press 'Show Chart' button */
                    chartType: Charts.ChartType.BAR;
                    onFinishedPaint: {
                      if (settings.dataType == "SIM") {
                          maxScaleGraph = settings.maxScaleYSIM;
                      }
                      rotateObj()
                    }
                }
                ChartInputArea {
                  id: inputArea
                  anchors.fill: parent
                  onTouchClick: {
                    if (x+units.gu(32)>settings.axisX) {
                      xbubble = settings.axisX-units.gu(32)
                    } else {
                      xbubble = x
                    }
                    ybubble=y
                    var nDays = DateUtils.getDifferenceInDays(firstDayMonth,lastDayMonth) + 1;
                    var gap = (settings.axisX - settings.errorX)/nDays;
                    var today = Math.ceil((x - settings.errorX)/gap);
                    var touchedDate = DateUtils.addDaysAndFormat(firstDayMonth, today-1)
                    var storedDateBytes = Storage.getBytesValueByDate(touchedDate, settings.dataType)
                    var prevTouchedDate = DateUtils.addDaysAndFormat(firstDayMonth, today-2)
                    var prevStoredDateBytes = Storage.getBytesValueByDate(prevTouchedDate, settings.dataType)
                    var dayData = storedDateBytes - prevStoredDateBytes
                    if (dayData < 0) {
                      dayData = storedDateBytes
                    }
                    var touchedBytes = Math.round(maxScaleGraph*(settings.axisY-settings.errorY1-y)/(settings.axisY-settings.errorY1-settings.errorY2))
                    if (touchedBytes>storedDateBytes || storedDateBytes==0 || touchedBytes<=0) {
                        notification.showNotification(
                                          i18n.tr("Graph gestures complete list available at app 'Information' page"),
                                          bkgColor, fontColor)
                    } else {
                      if (today>=1 && today<=lastDayMonth.getDate()) {
                        notification.showNotification(
                                        i18n.tr("Date:") + " " + touchedDate + "\n" +
                                        i18n.tr("Total data usage:") + " " + storedDateBytes.toFixed(1) + " MB" + "\n" +
                                        i18n.tr("Daily data usage:") + " " + dayData.toFixed(1) + " MB",
                                        bkgColor, fontColor)
                      }
                    }
                  }
                  onTouchPressAndHold: alternateAction(x, y);
                  onAlternateAction: {
                      var dataXYreal = maxScaleGraph*(settings.axisY-settings.errorY1-y)/(settings.axisY-settings.errorY1-settings.errorY2)
                      var dataXY = Math.round(dataXYreal)
                      if (dataXY>0 & dataXY<maxScaleGraph) {
                              if (dataModel.count>0) {
                                    var i = dataModel.count-1;
                                    var found = false;
                                    while (i>=0 & found == false) {
                                       if (i>0) {
                                          if (dataXY >= dataModel.get(i).bytes) {
                                            if (dataXY == dataModel.get(i).bytes) {
                                              found = true;
                                            }
                                            i = i - 1;
                                          } else {
                                            if (dataXY < dataModel.get(i).bytes & dataXY != dataModel.get(i-1).bytes) {
                                              dataModel.insert(i+1, {"bytes": dataXY});
                                              createObj(y, dataXY);
                                              found = true;
                                              var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                              if (dataXY<=totalDataEdit) {
                                                  StorageThresholds.updateNotifFlagValue(1, dataXY, settings.dataType)
                                              } else {
                                                  StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                                              }
                                            }
                                          }
                                       } else {
                                          if (dataXY > dataModel.get(i).bytes) {
                                            dataModel.insert(i, {"bytes": dataXY});
                                            createObj(y, dataXY)
                                          } else {
                                            if (dataXY < dataModel.get(i).bytes) {
                                                dataModel.insert(i+1, {"bytes": dataXY});
                                                createObj(y, dataXY)
                                            }
                                          }
                                          found = true;
                                          var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                          if (dataXY<=totalDataEdit) {
                                              StorageThresholds.updateNotifFlagValue(1, dataXY, settings.dataType)
                                          } else {
                                              StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                                          }
                                       }
                                    }
                              } else {
                                StorageThresholds.createTables(settings.dataType)
                                dataModel.append({"bytes": dataXY});
                                createObj(y, dataXY)
                                var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                if (dataXY<=totalDataEdit) {
                                    StorageThresholds.updateNotifFlagValue(1, dataXY, settings.dataType)
                                } else {
                                    StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                                }
                              }
                              Installdaemon.traslationNotif(i18n.tr("Data threshold for -"), i18n.tr("set at"), i18n.tr("was exceeded!"))
                        }
                  }
                  Text {
                          id: dataExistenceText
                          anchors.top: parent.top
                          anchors.topMargin: parent.height/3
                          anchors.left: parent.left
                          anchors.leftMargin: parent.width/2 - units.gu(10)
                          text: i18n.tr("Data not available");
                          color: fontColor
                          visible: isNaN(maxScaleGraph) ? true : false
                          font.pointSize: 30;
                  }
                  onTwoFingerSwipeYDetected: {
                      mBytesChartContainer.maxValue = maxScaleGraph + maxScaleGraph * steps / 15
                      if (settings.dataType == "WIFI") {
                         settings.maxScaleY = mBytesChartContainer.maxValue;
                         settings.maxScaleYStored = settings.maxScaleY;
                         maxScaleGraphStored = settings.maxScaleY;
                      } else {
                         settings.maxScaleYSIM = mBytesChartContainer.maxValue;
                         settings.maxScaleYSIMStored = settings.maxScaleYSIM;
                         maxScaleGraphStored = settings.maxScaleYSIM;
                      }
                      drawGraph(settings.dataType);
                  }
                  onSwipeXDetected: {
                      if (steps<=-5 & swipeAnimationSequence.running == false) {
                        var dateParcel = chartPage.targetDate.split("-");
                        var newDate = new Date(dateParcel[0], dateParcel[1], dateParcel[2]);
                        PageHeader.title = Qt.formatDateTime(newDate, "MMMM yyyy")
                        chartPage.targetDate = Qt.formatDateTime(newDate, "yyyy-MM-dd");
                        animationXfR = 0
                        animationXtR = -settings.axisX
                        animationXfL = settings.axisX
                        animationXtL = 0
                        swipeAnimationSequence.running = true
                      }
                      if (steps>=5 & swipeAnimationSequence.running == false) {
                        var dateParcel = chartPage.targetDate.split("-");
                        var newDate = new Date(dateParcel[0], dateParcel[1] - 2, dateParcel[2]);
                        PageHeader.title = Qt.formatDateTime(newDate, "MMMM yyyy")
                        chartPage.targetDate = Qt.formatDateTime(newDate, "yyyy-MM-dd");
                        animationXfR = 0
                        animationXtR = settings.axisX
                        animationXfL = -settings.axisX
                        animationXtL = 0
                        swipeAnimationSequence.running = true
                      }
                  }
                }
            }
        }
    }

    BottomEdgeHint {
        id: bottomEdgeHint
        text: i18n.tr("Data thresholds list")
        onClicked: revealBottomEdge()
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height
        contentComponent: Rectangle {
            width: bottomEdge.width
            height: bottomEdge.height
            color: bkgColor
            PageHeader {
                id: thresholdsPage
              title: i18n.tr("Data thresholds list")

              StyleHints	{
                foregroundColor: fontColor
                backgroundColor: bkgColor
                dividerColor: lineColor
              }
            }
            Label{
                id: noThreshAvaiLabel
                anchors.top: thresholdsPage.bottom
                anchors.centerIn: parent
                text: i18n.tr("No data thresholds available")
                textSize: Label.Large
                color: colorLabelThresh
            }
    		    ScrollView {
    			       anchors {
                           top: thresholdsPage.bottom
                           bottom: parent.bottom
                           right: parent.right
                           left: parent.left
                       }
    			       ListView {
                      id: listOfThresholds
              				anchors.fill: parent
              				model: dataModel
              				delegate:  ListItem {
                                    id: itemOfList
                                    contentItem.anchors {
                                        leftMargin: units.gu(5)
                                    }
    		                            height: units.gu(5)
                                    leadingActions: leading
    				                        trailingActions: trailing
    				                        ListItemLayout {
                          					    id: listItemLayout
                          					    title.text: modelData
                                        title.color: fontColor
                                        Label {
                                          color: fontColor
                                          text: index+1 + "."
                                          SlotsLayout.position: SlotsLayout.Leading;
                                        }
                                        Icon {
                                          id: audioIcon
                                          name: StorageThresholds.getMuteFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "audio-speakers-muted-symbolic" : "audio-speakers-symbolic"
                                          color: StorageThresholds.getMuteFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "red" : "green"
                                          SlotsLayout.position: SlotsLayout.Trailing;
                                          width: units.gu(2)
                                        }
                                        Icon {
                                          id: okIcon
                                          name: StorageThresholds.getNotifFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "ok" : ""
                                          SlotsLayout.position: SlotsLayout.Trailing-1;
                                          width: units.gu(2)
                                        }
                        				    }
                                    TextMetrics {
                                      id: textMetrics
                                      text: dataModel.get(index).bytes
                                    }
                                    TextMetrics {
                                      id: textMetricsPos
                                      text: index+1 + "."
                                    }
                                    SlotsLayout {
                                      id: layout
                                      mainSlot: Label {
                                          text: typeData
                                          color: fontColor
                                          SlotsLayout.padding.leading: textMetricsPos.advanceWidth + textMetrics.advanceWidth + units.gu(4)
                                      }
                                    }
                                    onContentMovementStarted: {
                                        listOfThresholds.currentIndex = index
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: listOfThresholds.currentIndex = index
                                    }
                                    ListItemActions {
                                        id: leading
                                        actions: Action {
                                            iconName: "delete"
                                            onTriggered: {
                                                  var found = false
                                                  var EOL = false
                                                  var i=0
                                                  var itemsNum = numberObj()
                                                  var index = listOfThresholds.currentIndex
                                                  while (i <= itemsNum-1 & found == false & EOL == false) {
                                                      var dataModelBytes = dataModel.get(listOfThresholds.currentIndex).bytes
                                                      var found = findAndDeleteObj(i, dataModelBytes);
                                                      if (i==itemsNum-1) {
                                                        EOL = true
                                                      }
                                                      i = i + 1
                                                  }
                                                  dataModel.remove(listOfThresholds.currentIndex, 1)
                                                  StorageThresholds.deleteThreshold(dataModelBytes, settings.dataType)
                                            }
                                        }
                                    }
                                    Item {
//                                      width: units.gu(80)
//                                      height: units.gu(80)
                                      Component {
                                           id: dialog
                                           Dialog {
                                               id: dialogue
                                               title: i18n.tr("Edit data threshold")
                                               text: i18n.tr("Only an integer type number is allowed in the text field.")
                                               TextField {
                                                  id: textFieldReal
                                                  text: dataModel.get(listOfThresholds.currentIndex).bytes
                                                  hasClearButton: false
                                               }
                                               TextField {
                                                  id: textFieldFake
                                                  visible: false
                                                  text: dataModel.get(listOfThresholds.currentIndex).bytes
                                                  hasClearButton: false
                                               }
                                               Button {
                                                    text: i18n.tr("Save")
                                                    color: UbuntuColors.green
                                                    onClicked: {
                                                          var editData = parseInt(textFieldReal.text)
                                                          var i=dataModel.count-1;
                                                          var found = false;
                                                          if (editData==0) {
                                                            found = true
                                                          } else {
                                                              while (i>=0 & found == false) {
                                                                if (dataModel.get(i).bytes==editData) {
                                                                  found = true
                                                                }
                                                                i = i-1;
                                                              }
                                                          }
                                                          if (!isNaN(editData)) {
                                                                if (!found) {
                                                                    StorageThresholds.updateThreshold(textFieldFake.text, textFieldReal.text, settings.dataType)
                                                                    if (editData<=totalDataEdit) {
                                                                        StorageThresholds.updateNotifFlagValue(true, textFieldReal.text, settings.dataType)
                                                                    } else {
                                                                        StorageThresholds.updateNotifFlagValue(false, textFieldReal.text, settings.dataType)
                                                                    }
                                                                    var foundObj = false
                                                                    var EOL = false
                                                                    var i=0
                                                                    var itemsNum = numberObj()
                                                                    while (i <= itemsNum-1 & foundObj == false & EOL == false) {
                                                                        var dataModelBytes = dataModel.get(index).bytes
                                                                        found = findAndDeleteObj(i, dataModelBytes);
                                                                        if (i==itemsNum-1) {
                                                                          EOL = true
                                                                        }
                                                                        i = i + 1
                                                                    }
                                                                    var yObj = (maxScaleGraph*(settings.axisY-settings.errorY1) - editData*(settings.axisY-settings.errorY1-settings.errorY2))/maxScaleGraph-25;
                                                                    createObj(yObj, editData);
                                                                    dataModel.setProperty(index, "bytes", editData)
                                                                    dataModel.remove(index, 1)
                                                                    dataModel.append({"bytes": editData});
                                                                    var found = false;
                                                                    var i=dataModel.count-1;
                                                                    while (i>=0 & found == false) {
                                                                        if (dataModel.get(i-1).bytes<dataModel.get(i).bytes) {
                                                                          dataModel.move(i,i-1,1);
                                                                          i = i-1;
                                                                        } else {
                                                                          found = true;
                                                                        }
                                                                    }
                                                                    PopupUtils.close(dialogue)
                                                                } else {
                                                                  messageError.visible = true
                                                                  if (editData==0) {
                                                                    messageError.text=i18n.tr("ERROR: zero figure (0) is not allowed. Correct it or press the 'Cancel' button to go back to the thresholds list.");
                                                                  } else {
                                                                    messageError.text=i18n.tr("ERROR: number is already in the list. Modify it or press the 'Cancel' button to go back to the thresholds list.");
                                                                  }
                                                                }
                                                        } else {
                                                          messageError.visible = true
                                                          messageError.text=i18n.tr("ERROR: typed figure is not a number. Correct it or press the 'Cancel' button to go back to the thresholds list.");
                                                        }
                                                    }
                                               }
                                               Button {
                                                    text: i18n.tr("Cancel")
                                                    color: UbuntuColors.graphite
                                                    onClicked: PopupUtils.close(dialogue)
                                               }
                                               Label {
                                                  id: messageError
                                                  width: parent.width
                                                  horizontalAlignment: Text.AlignLeft
                                                  wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                                                  visible: false
                                                  text: " "
                                                  color: UbuntuColors.red
                                               }
                                           }
                                      }
                                    }
                                    ListItemActions {
                                        id: trailing
                                        actions: [
                                            Action {
                                              iconName: "notification"
                                              parameterType: Action.Bool
                                              onTriggered: {
                                                    if (audioIcon.name == "audio-speakers-symbolic") {
                                                      StorageThresholds.updateMutedValue(1, dataModel.get(index).bytes, settings.dataType)
                                                      audioIcon.name = "audio-speakers-muted-symbolic"
                                                      audioIcon.color = "red"
                                                    } else {
                                                        if (audioIcon.name == "audio-speakers-muted-symbolic") {
                                                            StorageThresholds.updateMutedValue(0, dataModel.get(index).bytes, settings.dataType)
                                                            audioIcon.name = "audio-speakers-symbolic"
                                                            audioIcon.color = "green"
                                                        }
                                                    }
                                              }
                                            },
                                            Action {
                                              iconName: "edit"
                                              parameterType: Action.Bool
                                              onTriggered: {
                                                  PopupUtils.open(dialog)
                                              }
                                            }
                                        ]
                                    }
    				                    }
                      currentIndex : -1
                      move: Transition {
                          id: moveTrans
                              NumberAnimation { properties: "x,y"; duration: 800; easing.type: Easing.OutBack }
                      }
                      displaced: Transition {
                          NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
                      }
    			       }
              }
        }
        onCommitStarted: {
          if (dataModel.count==0) {
                colorLabelThresh=fontColor;
              } else {
                colorLabelThresh = "transparent";
              }
        }
}


    function drawGraph(connType) {
             /* extract the year, month, day from the variable 'targetDate' that contains a value like yyyy-mm-dd */
             var dateParts = chartPage.targetDate.split("-");

             /* build a JS Date Object using string tokens (month is 0-based) */
             var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);

             /* calculates first and last day of the month */
             firstDayMonth = new Date( date.getFullYear(),date.getMonth(), 1);
             lastDayMonth = new Date( date.getFullYear(), date.getMonth() + 1, 0);

             /* set the data-set at the chart and make visible the chart and legend */
             mBytesChart.chartData = ChartUtils.getChartData(firstDayMonth,lastDayMonth,connType,lineColor,barColor);

             mBytesChartContainer.visible = true;
    }

    function createObj(y, data) {
        var i = chartPage.objects.length
        var component = Qt.createComponent("DataPlaceHolder.qml");
        chartPage.objects[i] = component.createObject(inputArea);
        chartPage.objects[i].x = settings.errorX+summitArrowOffset;
        chartPage.objects[i].y = y-25;
        chartPage.objects[i].bytesInfo = data;
        chartPage.objects[i].colorEdge = lineColor;
    }

    function findAndDeleteObj(i, confData) {
        var match = false
        if (chartPage.objects[i].bytesInfo == confData) {
          chartPage.objects[i].destroy()
          match = true
        }
        return match
    }

    function numberObj() {
      var items = chartPage.objects.length
      return items
    }

    function rotateObj() {
        var yAxis = new Array(chartPage.objects.length)
        if (maxScaleGraph>0) {
            if (chartPage.objects.length>0) {
                for (var i=0; i < chartPage.objects.length; i++) {
                      chartPage.objects[i].y = (maxScaleGraph*(settings.axisY-settings.errorY1) - chartPage.objects[i].bytesInfo*(settings.axisY-settings.errorY1-settings.errorY2))/maxScaleGraph-25
                      chartPage.objects[i].x = settings.errorX+summitArrowOffset;
                      if (chartPage.objects[i].bytesInfo > maxScaleGraph & chartPage.objects[i].colorEdge != "transparent") {
                        chartPage.objects[i].colorEdge = "transparent"
                      } else {
                        if (chartPage.objects[i].bytesInfo < maxScaleGraph & chartPage.objects[i].colorEdge == "transparent") {
                                chartPage.objects[i].colorEdge = lineColor
                        }
                      }
                  }
            }
        } else {
            if (chartPage.objects.length>0) {
                for (var i=0; i < chartPage.objects.length; i++) {
                    chartPage.objects[i].colorEdge = "transparent";
                }
            }
        }
    }

    function initOrdDataModel(value) {
        if (dataModel.count>0) {
              var i = dataModel.count-1;
              var found = false;
              while (i>=0 & found == false) {
                 if (i>0) {
                    if (value >= dataModel.get(i).bytes) {
                      if (value == dataModel.get(i).bytes) {
                        found = true;
                      }
                      i = i - 1;
                    } else {
                      if (value < dataModel.get(i).bytes & value != dataModel.get(i-1).bytes) {
                          dataModel.insert(i+1, {"bytes": value});
                        found = true;
                      }
                    }
                 } else {
                    if (value > dataModel.get(i).bytes) {
                        dataModel.insert(i, {"bytes": value});
                    } else {
                      if (value < dataModel.get(i).bytes) {
                          dataModel.insert(i+1, {"bytes": value});
                      }
                    }
                    found = true;
                 }
              }
        } else {
            dataModel.append({"bytes": value});
        }
    }

}
