  function getDatabase(connType) {
     if (connType=="WIFI") {
       return LocalStorage.openDatabaseSync("thresholds_db", "1.0", "StorageDatabase", 1000000);
     } else if (connType=="SIM") {
       return LocalStorage.openDatabaseSync("thresholds_db_SIM", "1.0", "StorageDatabase", 1000000);
     }
  }

  function createTables(connType) {

      var db = getDatabase(connType);

      db.transaction(
          function(tx) {
              tx.executeSql('CREATE TABLE IF NOT EXISTS chartYdata(threshold_id INTEGER PRIMARY KEY AUTOINCREMENT, data_thresholds REAL, silenced_flag INTEGER, notified_flag INTEGER)');
      });
  }

  function getMuteFlagFromThreshold(value, connType){

        var db = getDatabase(connType);

        var rs = "";

        db.transaction(function(tx) {
            rs = tx.executeSql("SELECT silenced_flag FROM chartYdata t where t.data_thresholds = ?;", [value]);
            }
        );

        if (rs.rows.length > 0) {
            var row = rs.rows.length - 1;
            return rs.rows.item(row).silenced_flag;
        } else {
            return 0;
        }
   }

   function getNotifFlagFromThreshold(value, connType){

         var db = getDatabase(connType);

         var rs = "";

         db.transaction(function(tx) {
             rs = tx.executeSql("SELECT notified_flag FROM chartYdata t where t.data_thresholds = ?;", [value]);
             }
         );

         if (rs.rows.length > 0) {
             var row = rs.rows.length - 1;
             return rs.rows.item(row).notified_flag;
         } else {
             return 0;
         }
    }

 function getNotifFlagFromPos(position, connType){

       var db = getDatabase(connType);

       var rs = "";

       db.transaction(function(tx) {
           rs = tx.executeSql("SELECT notified_flag FROM chartYdata t where t.data_thresholds");
           }
       );

       if (rs.rows.length > 0) {
           var row = position;
           return rs.rows.item(row).notified_flag;
       } else {
           return 0;
       }
  }

  function getValueFromPos(position, connType){

        var db = getDatabase(connType);

        var rs = "";

        db.transaction(function(tx) {
            rs = tx.executeSql("SELECT data_thresholds FROM chartYdata t where t.data_thresholds");
            }
        );

        if (rs.rows.length > 0) {
            var row = position;
            return rs.rows.item(row).data_thresholds;
        } else {
            return 0;
        }
   }

     function getMuteFromPos(position, connType){

           var db = getDatabase(connType);

           var rs = "";

           db.transaction(function(tx) {
                  rs = tx.executeSql("SELECT silenced_flag FROM chartYdata t where t.data_thresholds");
               }
           );

           if (rs.rows.length > 0) {
              var row = position;
               return rs.rows.item(row).silenced_flag;
           } else {
               return 0;
           }
      }

     function getNumData(connType){

           var db = getDatabase(connType);

           var rs = "";

           db.transaction(function(tx) {
                 rs = tx.executeSql("SELECT data_thresholds FROM chartYdata t where t.data_thresholds");
               }
           );

           if (rs.rows.length > 0) {
             return rs.rows.length;
           } else {
               return 0;
           }
      }

   function insertThresholdValue(value, connType){

       var db = getDatabase(connType);
       var res = "";

       db.transaction(function(tx) {

           var rs = tx.executeSql('INSERT INTO chartYdata(data_thresholds,silenced_flag,notified_flag) VALUES (?,?,?);', [value,0,0]);
           if (rs.rowsAffected > 0) {
               res = "OK";
           } else {
               res = "Error";
           }
       }
       );
       return res;
  }

function deleteThreshold(value, connType){

     var db = getDatabase(connType);

     var rs = "";

     db.transaction(function(tx) {
      rs = tx.executeSql("DELETE FROM chartYdata WHERE data_thresholds = ?", [value]);
         }
     );

}

function updateThreshold(valueOld, valueNew, connType){

   var db = getDatabase(connType);

   var rs = "";

   db.transaction(function(tx) {
        rs = tx.executeSql('UPDATE chartYdata SET data_thresholds = ? WHERE data_thresholds = ?', [valueNew, valueOld]);

      }
   );

 }

function updateMutedValue(silflag, value, connType){

   var db = getDatabase(connType);

   var rs = "";

   db.transaction(function(tx) {
        rs = tx.executeSql('UPDATE chartYdata SET silenced_flag = ? WHERE data_thresholds = ?', [silflag, value]);

      }
   );

 }

 function updateNotifFlagValue(notifflag, value, connType){

    var db = getDatabase(connType);

    var rs = "";

    db.transaction(function(tx) {
         rs = tx.executeSql('UPDATE chartYdata SET notified_flag = ? WHERE data_thresholds = ?', [notifflag, value]);

       }
    );

  }
