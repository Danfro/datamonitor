import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Readconns 1.0

Page {
    id: simDetailsPage

    property var simNames : [];
    property var carrierNames : [];
    property var deviceNames : [];
    property var simDevices: [];
    property bool doubleSim: false

    header: PageHeader {
        title: i18n.tr("SIM details")

        StyleHints	{
          foregroundColor: fontColor
          backgroundColor: bkgColor
          dividerColor:	lineColor
        }
    }

    Component.onCompleted: {
        deviceNames = Readconns.deviceList()
        simDevices = Readconns.simSelector(deviceNames)
        if (simDevices.length > 1) {
          doubleSim = true;
        }
        simNames = Readconns.SIMconns()
        carrierNames = Readconns.nameCarrier()
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    ScrollView {
        id: scrollView
        anchors {
            top: simDetailsPage.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        Column {
            id: simDetailsColumn
            spacing: units.gu(2)
            width: scrollView.width

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    title {
                        text: "SIM-1:"
                        color: fontColor
                        textSize: Label.Medium
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: generalName
                    title {
                        text: i18n.tr("General device name:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelGenName
                        text: simDevices[0]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: deviceName
                    title {
                        text: i18n.tr("Device name:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelDevName
                        text: simNames[0]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                ListItemLayout {
                    id: carrierName
                    title {
                        text: i18n.tr("Carrier name:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelCarName
                        text: carrierNames[0]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: doubleSim
                ListItemLayout {
                    id: bearerName
                    title {
                        text: i18n.tr("Bearer channel:")
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelBearName
                        text: carrierNames[1]
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    title {
                        text: "SIM-2:"
                        color: fontColor
                        textSize: Label.Medium
                    }
                }
            }

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    id: generalName2
                    title {
                        text: doubleSim ? generalName.text : ""
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelGenName2
                        text: doubleSim ? simDevices[1] : ""
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    id: deviceName2
                    title {
                        text: doubleSim ? deviceName.text : ""
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelDevName2
                        text: doubleSim ? simNames[1] : ""
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    id: carrierName2
                    title {
                        text: doubleSim ? carrierName.text : ""
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelCarName2
                        text: doubleSim ? carrierNames[2] : ""
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

            ListItem {
                divider.visible: false
                visible: doubleSim
                ListItemLayout {
                    id: bearerName2
                    title {
                        text: doubleSim ? bearerName.text : ""
                        color: fontColor
                        textSize: Label.Medium
                    }
                    Label {
                        id: labelBearName2
                        text: doubleSim ? carrierNames[3] : ""
                        textSize: Label.Medium
                        color: fontColor
                        SlotsLayout.position: SlotsLayout.Trailing
                    }
                }
            }

        }
    }
}
