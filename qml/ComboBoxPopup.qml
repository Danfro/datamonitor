/*
 * Copyright (C) 2016 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored-by: Florian Boucault <florian.boucault@canonical.com>
 * Modified-by: Matteo Bellei <mattbel10@hotmail.com> dated 09 March 2021
 */
import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import "StorageThemes.js" as StorageThemes

Popover {
    id: comboBoxPopup

    implicitHeight: 5.5 * itemHeight
    contentHeight: Math.min(implicitHeight, listView.count * itemHeight)
    callerMargin: -units.gu(1)

    property var model
    property real itemHeight
    property real itemMargins
    property string textRole
    property ComboBox comboBox
    property var bindingTarget
    property string bindingProperty
    property string bindingPropertyIndex

    property bool square: true
    property real selectedItemY: currentIndex * itemHeight

    ScrollView {
        width: comboBoxPopup.contentWidth
        height: comboBoxPopup.contentHeight

        ListView {
            id: listView
            width: comboBoxPopup.contentWidth
            height: comboBoxPopup.contentHeight
            model: comboBoxPopup.model
            clip: true
            currentIndex: comboBoxPopup.comboBox.currentIndex
            delegate: MouseArea {
                id: mouseArea
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: comboBoxPopup.itemHeight
                onClicked: {
                    comboBoxPopup.comboBox.currentIndex = index
                    bindingTarget[bindingProperty] = comboBoxPopup.model.get(index)[textRole]
                    bindingTarget[bindingPropertyIndex] = index
                    var newthemeprops = StorageThemes.getThemeFromName(bindingTarget[bindingProperty])
                    settings.backgrndColorCust = newthemeprops[0]
                    settings.fontColorCust = newthemeprops[1]
                    settings.lineColorCust = newthemeprops[2]
                    settings.buttonColorCust = newthemeprops[3]
                    settings.gridColorCust = newthemeprops[4]
                    settings.barColorCust = newthemeprops[5]
                    settings.opac1 = newthemeprops[6]
                    settings.opac2 = newthemeprops[7]
                    settings.opac3 = newthemeprops[8]
                    settings.opac4 = newthemeprops[9]
                    settings.opac5 = newthemeprops[10]
                    settings.opac6 = newthemeprops[11]
                    settings.backgroundColorIndex = newthemeprops[12]
                    settings.fontColorIndex = newthemeprops[13]
                    settings.lineColorIndex = newthemeprops[14]
                    settings.buttonColorIndex = newthemeprops[15]
                    settings.gridColorIndex = newthemeprops[16]
                    settings.barColorIndex = newthemeprops[17]
                    root.themeColorSelection()
                    settings.isLastThemeSaved = true
                    comboBox.currentText = bindingTarget[bindingProperty]
                    saveThemesButton.text = prefPage.manTheme + "..."
                    PopupUtils.close(comboBoxPopup);
                }

                hoverEnabled: true

                Rectangle {
                  color: fontColor
                  anchors.fill: parent
                  Rectangle {
                      visible: mouseArea.containsMouse
                      anchors.fill: parent
                      color: "red"
                      border.width: units.dp(1)
                      border.color: Qt.darker(color, 1.02)
                      antialiasing: true
                  }

                  Rectangle {
                      visible: index == currentIndex
                      x: comboBoxPopup.itemMargins / 2 - width / 2
                      anchors.verticalCenter: parent.verticalCenter
                      width: units.gu(0.5)
                      height: width
                      color: bkgColor
                      antialiasing: true
                      radius: width/2
                  }

                  Label {
                      id: label
                      anchors {
                          fill: parent
                          leftMargin: comboBoxPopup.itemMargins
                          rightMargin: comboBoxPopup.itemMargins
                      }
                      text: model[textRole]
                      elide: Text.ElideRight
                      verticalAlignment: Text.AlignVCenter
                      color: bkgColor

                  }
                }
            }
        }
    }
}
