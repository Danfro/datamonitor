#!/bin/bash

set -e

rm /home/phablet/.config/upstart/mbellei-indicator-datamonitor.conf
rm /home/phablet/.local/share/unity/indicators/com.mbellei.indicator.datamonitor

echo "indicator-datamonitor uninstalled"
