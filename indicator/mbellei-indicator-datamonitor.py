import sys
import os
import json
import subprocess
import shlex
import logging
from subprocess import Popen, PIPE
from gi.repository import Gio
from gi.repository import GLib
from configparser import ConfigParser


import gettext
t = gettext.translation('datamonitor.matteobellei', fallback=True, localedir='/opt/click.ubuntu.com/datamonitor.matteobellei/current/share/locale/')  # TODO don't hardcode this
_ = t.gettext

BUS_NAME = "com.mbellei.indicator.datamonitor"
BUS_OBJECT_PATH = "/com/mbellei/indicator/datamonitor"
BUS_OBJECT_PATH_PHONE = BUS_OBJECT_PATH + "/phone"

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

class dataMonitorIndicator(object):
    ROOT_ACTION = 'root'
    SETTINGS_ACTION = 'settings'
    AUTO_ACTION = 'toggle'
    MAIN_SECTION = 0

    config_fileSIM = "/home/phablet/.config/datamonitor.matteobellei/configSIM.json"  # TODO don't hardcode this
    config_fileWIFI = "/home/phablet/.config/datamonitor.matteobellei/configWIFI.json"  # TODO don't hardcode this
    config_dateTime = "/home/phablet/.config/datamonitor.matteobellei/dateTime.json"  # TODO don't hardcode this
    config_fileToggles = "/home/phablet/.config/datamonitor.matteobellei/datamonitor.matteobellei.conf"  # TODO don't hardcode this
    config_visibleIcon = "/home/phablet/.config/datamonitor.matteobellei/isIconHidden.json"  # TODO don't hardcode this
    refresh_sec = 10
    config_parser = ConfigParser()

    def __init__(self, bus):
        self.bus = bus
        self.action_group = Gio.SimpleActionGroup()
        self.menu = Gio.Menu()
        self.sub_menu = Gio.Menu()
        self.get_config()

    def get_config(self):
            try:
                f = open(self.config_fileWIFI)
                config_json = json.load(f)
                self.today_data_WIFI = float(config_json['today_data_WIFI'].strip())
                self.total_data_WIFI = float(config_json['total_data_WIFI'].strip())
                f.close()
            except:
                self.today_data_WIFI = 0
                self.total_data_WIFI = 0

            try:
                f = open(self.config_fileSIM)
                config_json = {}
                config_json = json.load(f)
                self.today_data_SIM = float(config_json['today_data_SIM'].strip())
                self.total_data_SIM = float(config_json['total_data_SIM'].strip())
                f.close()
            except:
                self.today_data_SIM = 0
                self.total_data_SIM = 0

            try:
                f = open(self.config_dateTime)
                config_json = json.load(f)
                self.today_dateTime = config_json['dateTime'].strip()
                f.close()
            except:
                self.today_dateTime = _('NOT AVAILABLE')


    def autoSwitchEnabledSIMtoday(self):
        try:
            self.config_parser.read(self.config_fileToggles)
            general_config = self.config_parser["Indicator"]
            return general_config['simtoggleToday'].strip()
        except:
            return "true"

    def autoSwitchEnabledSIMtotal(self):
        try:
            self.config_parser.read(self.config_fileToggles)
            general_config = self.config_parser["Indicator"]
            return general_config['simtoggleTotal'].strip()
        except:
            return "true"

    def autoSwitchEnabledWIFItoday(self):
        try:
            self.config_parser.read(self.config_fileToggles)
            general_config = self.config_parser["Indicator"]
            return general_config['wifitoggleToday'].strip()
        except:
            return "true"

    def autoSwitchEnabledWIFItotal(self):
        try:
            self.config_parser.read(self.config_fileToggles)
            general_config = self.config_parser["Indicator"]
            return general_config['wifitoggleTotal'].strip()
        except:
            return "true"

    def autoSwitchEnabledVisible(self):
        try:
            f = open(self.config_visibleIcon)
            config_json = json.load(f)
            self.isIconHidden = config_json['isIconHidden'].strip()
            if self.isIconHidden == "True":
                exit_status = True
            elif self.isIconHidden == "False":
                exit_status = False
            f.close()
            return exit_status

        except:
            return False

    def toggleAuto(self):
        autoState = self.autoSwitchEnabledVisible()

        if autoState == True:
           myJSON = json.dumps({"isIconHidden" : "False"})
        elif autoState == False:
           myJSON = json.dumps({"isIconHidden" : "True"})

        with open(self.config_visibleIcon, 'w') as conf:
            conf.write(myJSON)
        conf.close()

        return autoState

    def auto_mode_activated(self, action, data):
         ishidden = self.toggleAuto()
         self.action_group.change_action_state(self.ROOT_ACTION, self.root_state())
         self.action_group.change_action_state(self.AUTO_ACTION, GLib.Variant.new_boolean(not ishidden))


    def isactiveDaemon(self):
        status_daemon = subprocess.Popen(shlex.split('status mattdaemon'),stdout = subprocess.PIPE).communicate()
        findStr = str(status_daemon).find("stop")
        if findStr != -1:
            result = "stop"
        else:
            findStr = str(status_daemon).find("start")
            if findStr != -1:
                result = "start"
            else:
                result = "unknown"
        return result

    def settings_action_activated(self, action, data):
        logger.debug('settings_action_activated')
        subprocess.Popen(shlex.split('ubuntu-app-launch datamonitor.matteobellei_datamonitor_0.2.3'))

    def _setup_actions(self):
        root_action = Gio.SimpleAction.new_stateful(self.ROOT_ACTION, None, self.root_state())
        self.action_group.insert(root_action)

        settings_action = Gio.SimpleAction.new(self.SETTINGS_ACTION, None)
        settings_action.connect('activate', self.settings_action_activated)
        self.action_group.insert(settings_action)

        auto_action = Gio.SimpleAction.new_stateful(self.AUTO_ACTION, None, GLib.Variant.new_boolean(self.autoSwitchEnabledVisible()))
        auto_action.connect('activate', self.auto_mode_activated)
        self.action_group.insert(auto_action)

    def _create_section(self):
        self.get_config()
        section = Gio.Menu()

        auto_menu_item = Gio.MenuItem.new(_('Hide indicator icon'), 'indicator.{}'.format(self.AUTO_ACTION))
        auto_menu_item.set_attribute_value('x-canonical-type', GLib.Variant.new_string('com.canonical.indicator.switch'))
        section.append_item(auto_menu_item)

        isactiveDaemon = self.isactiveDaemon()

        if isactiveDaemon == "start":

            settings_menu_item = Gio.MenuItem.new(_('Status: ACTIVE'))
            section.append_item(settings_menu_item)

        elif isactiveDaemon == "stop":

            settings_menu_item = Gio.MenuItem.new(_('Status: INACTIVE'))
            section.append_item(settings_menu_item)

        elif isactiveDaemon == "unknown":

            settings_menu_item = Gio.MenuItem.new(_('Status: UNKNOWN'))
            section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('Updated:') + ' ' + str(self.today_dateTime))
        section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('dataMonitor\'s data usage information:'))
        section.append_item(settings_menu_item)

        if self.autoSwitchEnabledSIMtoday() == "true" or self.autoSwitchEnabledSIMtotal() == "true":

            settings_menu_item = Gio.MenuItem.new(_('SIM DATA USAGE:'))
            section.append_item(settings_menu_item)

            if self.autoSwitchEnabledSIMtoday() == "true":

                settings_menu_item = Gio.MenuItem.new(_('Today data: ') + str(self.today_data_SIM) + ' MB')
                section.append_item(settings_menu_item)

            if self.autoSwitchEnabledSIMtotal() == "true":

                settings_menu_item = Gio.MenuItem.new(_('Total month data: ') + str(self.total_data_SIM) + ' MB')
                section.append_item(settings_menu_item)

            settings_menu_item = Gio.MenuItem.new((' '))
            section.append_item(settings_menu_item)

        if self.autoSwitchEnabledWIFItoday() == "true" or self.autoSwitchEnabledWIFItotal() == "true":

            settings_menu_item = Gio.MenuItem.new(_('WI-FI DATA USAGE:'))
            section.append_item(settings_menu_item)

            if self.autoSwitchEnabledWIFItoday() == "true":

                settings_menu_item = Gio.MenuItem.new(_('Today data: ') + str(self.today_data_WIFI) + ' MB')
                section.append_item(settings_menu_item)

            if self.autoSwitchEnabledWIFItotal() == "true":

                settings_menu_item = Gio.MenuItem.new(_('Total month data: ') + str(self.total_data_WIFI) + ' MB')
                section.append_item(settings_menu_item)

            settings_menu_item = Gio.MenuItem.new((' '))
            section.append_item(settings_menu_item)

        settings_menu_item = Gio.MenuItem.new(_('dataMonitor App and Settings'), 'indicator.{}'.format(self.SETTINGS_ACTION))
        section.append_item(settings_menu_item)

        return section

    def _setup_menu(self):
        self.sub_menu.insert_section(self.MAIN_SECTION, 'dataMonitor', self._create_section())

        root_menu_item = Gio.MenuItem.new('dataMonitor', 'indicator.{}'.format(self.ROOT_ACTION))
        root_menu_item.set_attribute_value('x-canonical-type', GLib.Variant.new_string('com.canonical.indicator.root'))
        root_menu_item.set_submenu(self.sub_menu)
        self.menu.append_item(root_menu_item)

    def _update_menu(self):
        self.sub_menu.remove(self.MAIN_SECTION)
        self.sub_menu.insert_section(self.MAIN_SECTION, 'dataMonitor', self._create_section())
        return True

    def run(self):
        self._setup_actions()
        self._setup_menu()

        self.bus.export_action_group(BUS_OBJECT_PATH, self.action_group)
        self.menu_export = self.bus.export_menu_model(BUS_OBJECT_PATH_PHONE, self.menu)

        GLib.timeout_add_seconds(self.refresh_sec, self._update_menu)
        self._update_menu()


    def root_state(self):
        vardict = GLib.VariantDict.new()
        currentState = self.autoSwitchEnabledVisible()

        vardict.insert_value('visible', GLib.Variant.new_boolean(not currentState))

        vardict.insert_value('title', GLib.Variant.new_string(_('dataMonitor')))

        folder = Gio.File.new_for_path("/opt/click.ubuntu.com/datamonitor.matteobellei/current/assets/dMindicator.svg")
        icon = Gio.FileIcon.new(folder)

        vardict.insert_value('icon', icon.serialize())

        return vardict.end()

if __name__ == '__main__':
    bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(bus, 0, None, 'org.freedesktop.DBus', '/org/freedesktop/DBus', 'org.freedesktop.DBus', None)
    result = proxy.RequestName('(su)', BUS_NAME, 0x4)
    if result != 1:
        logger.critical('Error: Bus name is already taken')
        sys.exit(1)

    wi = dataMonitorIndicator(bus)
    wi.run()
    logger.debug('dataMonitor Indicator startup completed')
    GLib.MainLoop().run()
